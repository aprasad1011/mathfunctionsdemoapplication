package com.example.demo.validator;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Test;
import static com.example.demo.constants.ApplicationConstants.POSITIVE_NUMBER_PREDICATE_FAILURE_MESSAGE;

public class AddNumbersValidatorTest {

	AddNumbersValidator target = new AddNumbersValidator();

	@Test
	public void test_positiveNumberPredicateFailureMessage_success() {
		assertEquals(POSITIVE_NUMBER_PREDICATE_FAILURE_MESSAGE,
				AddNumbersValidator.positiveNumberPredicateFailureMessage());
	}

	@SuppressWarnings("serial")
	@Test
	public void test_validate_success() {
		assertEquals(Optional.empty(), target.validate(new ArrayList<Integer>() {
			{
				add(10);
				add(34);
				add(15);
			}
		}));
	}

	@SuppressWarnings("serial")
	@Test
	public void test_validate_validationError() {
		assertEquals(Optional.of(POSITIVE_NUMBER_PREDICATE_FAILURE_MESSAGE), target.validate(new ArrayList<Integer>() {
			{
				add(10);
				add(34);
				add(-15);
			}
		}));
	}
}
