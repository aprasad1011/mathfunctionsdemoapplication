package com.example.demo.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;

import org.junit.Test;

public class MathFunctionServicesTest {
	
	MathFunctionServices target = new MathFunctionServices();
	
	@Test
	public void test_addTwoNumbers_success() {
		assertEquals(30, target.addTwoNumbers(10, 20));
		assertNotEquals(20, target.addTwoNumbers(10, 20));
	}
	
	@Test
	public void test_addNumbers_success() {
		assertEquals(59, target.addNumbers(new ArrayList<Integer>() {{
			add(10);
			add(34);
			add(15);
		}}));
	}
	
	@Test
	public void test_substractTwoNumbers_success() {
		assertEquals(10, target.substractTwoNumbers(20,30));
		assertEquals(10, target.substractTwoNumbers(25,15));
	}
}
