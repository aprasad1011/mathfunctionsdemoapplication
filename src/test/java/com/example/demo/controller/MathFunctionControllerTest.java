package com.example.demo.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import javax.ws.rs.core.MediaType;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import static com.example.demo.constants.ApplicationConstants.POSITIVE_NUMBER_PREDICATE_FAILURE_MESSAGE;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MathFunctionControllerTest {

	MockMvc mockMvc;

	@Autowired
	protected WebApplicationContext wac;
	
	@Autowired
	MathFunctionController mathFunctionController;
	
	@Before
	public void setup() {
		this.mockMvc = standaloneSetup(this.mathFunctionController).build();
	}
	
	@Test
	public void test_monitoring_success() throws Exception {
		mockMvc.perform(get("/api/v1/monitoring").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", is("Application Is Working")));
	}
	
	@Test
	public void test_addTwoNumbers_success() throws Exception {
		mockMvc.perform(get("/api/v1/addTwoNumbers?num1=12&num2=13").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.entity", is(25)));
	}
	
	@Test
	public void test_addNumbers_success() throws Exception {
		mockMvc.perform(get("/api/v1/addNumbers?num=12&num=13&num=25").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.entity", is(50)));
	}
	
	@Test
	public void test_addNumbers_validationError() throws Exception {
		mockMvc.perform(get("/api/v1/addNumbers?num=12&num=13&num=-25").contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.status", is(400)))
        .andExpect(jsonPath("$.entity", is(POSITIVE_NUMBER_PREDICATE_FAILURE_MESSAGE)));
	}
	
	@Test
	public void test_substractTwoNumbers_success() throws Exception {
		mockMvc.perform(get("/api/v1/substractTwoNumbers?num1=12&num2=13").contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.status", is(200)))
        .andExpect(jsonPath("$.entity", is(1)));
	}
	
	@Test
	public void test_substractTwoNumbers_2_success() throws Exception {
		mockMvc.perform(get("/api/v1/substractTwoNumbers?num1=20&num2=13").contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.status", is(200)))
        .andExpect(jsonPath("$.entity", is(7)));
	}
	
}
