package com.example.demo.validator;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import org.springframework.stereotype.Component;

@Component
public class AddNumbersValidator {

	public Optional<String> validate(List<Integer> numList) {
		return validateForPositiveNumbers(numList, positiveNumberPredicate(), positiveNumberPredicateFailureMessage());
	}
	
	private Optional<String> validateForPositiveNumbers(List<Integer> numList, Predicate<Integer> predicate, String message ) {
		boolean isValid = numList.stream().allMatch(predicate);
		return isValid? Optional.empty() : Optional.of(message);
	}

	public static Predicate<Integer> positiveNumberPredicate(){
		return num-> num >= 0;
	}
	
	public static String positiveNumberPredicateFailureMessage(){
		return "Input list contains negative integer";
	}
}
