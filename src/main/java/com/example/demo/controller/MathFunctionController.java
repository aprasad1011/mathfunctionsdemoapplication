package com.example.demo.controller;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.MathFunctionServices;
import com.example.demo.validator.AddNumbersValidator;

@RestController
@RequestMapping("/api/v1")
public class MathFunctionController {
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private MathFunctionServices mathFunctionService;
	
	@Autowired
	private AddNumbersValidator addNumbersValidator;
	
	@GetMapping(value = "/monitoring")
	public String monitoring() {
		LOGGER.info("Request received for application test");
		return "Application Is Working";
	}

	@GetMapping(value = "/addTwoNumbers")
	public Response addTwoNumbers(@RequestParam("num1") Integer num1, @RequestParam("num2") Integer num2) {
		LOGGER.info("received Request for addition of numbers. num1: " + num1 + " , num2: " + num2);
		int result = mathFunctionService.addTwoNumbers(num1, num2);
		return Response.status(200).entity(result).build();
	}
	
	@GetMapping(value = "/substractTwoNumbers")
	public Response substractTwoNumbers(@RequestParam("num1") Integer num1, @RequestParam("num2") Integer num2) {
		LOGGER.info("received Request for substraction of numbers. num1: " + num1 + " , num2: " + num2);
		int result = mathFunctionService.substractTwoNumbers(num1, num2);
		return Response.status(200).entity(result).build();
	}
	
	@GetMapping(value = "/addNumbers")
	public Response addNumbers(@RequestParam("num") List<Integer> numList) {
		LOGGER.info("received Request for addition of n numbers. num: " + numList);
		return addNumbersValidator.validate(numList).map(buildErrorResponse()).orElseGet(getTotal(numList));
	}
	
	private Supplier<Response> getTotal(List<Integer> numList) {
		return()->{
			LOGGER.info("Returning success response with status 200");
			return Response.status(200).entity(mathFunctionService.addNumbers(numList)).build();
		};
	}

	private Function<String, Response> buildErrorResponse() {
		return errMsg -> {
			LOGGER.info("Error occured while validating input : " +errMsg);
			LOGGER.info("Returning validation error response with status 400");
			return Response.status(400).entity(errMsg).build();
		};
	}
}
