package com.example.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class MathFunctionServices {
	public int addTwoNumbers(int num1, int num2) {
		return num1 + num2;
	}

	public int substractTwoNumbers(int num1, int num2) {
		return Math.abs(num2 - num1);
	}

	public int addNumbers(List<Integer> numList) {
		return numList.stream().reduce(Integer::sum).orElseGet(() -> 0);
	}
}